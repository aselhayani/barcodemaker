﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BarcodeMaker
{
    static class Program
    {

        [DllImport("shell32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern void SHChangeNotify(uint wEventId, uint uFlags, IntPtr dwItem1, IntPtr dwItem2);
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //if (!IsAassociated())
              //  Associate();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            if (args.Count() >= 1)
                Application.Run(new frm_Barcode(args[0]));
            else
                Application.Run(new frm_Barcode());
        }
        public static bool IsAassociated()
        {

            return (Registry.CurrentUser.OpenSubKey("Software\\Classes\\.bar", false) != null);
        }
        public static void Associate()
        {
            //AppDomain.CurrentDomain.BaseDirectory
            RegistryKey FileReg = Registry.CurrentUser.CreateSubKey("Software\\Classes\\.bar");
            RegistryKey AppReg  = Registry.CurrentUser.CreateSubKey("Software\\Classes\\Applecations\\BarcodeMaker.exe");
            RegistryKey AppASSC = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrrentVersion\\Explorer\\FileExts\\.bar");
            FileReg.CreateSubKey("DefaultIcon").SetValue("",AppDomain.CurrentDomain.BaseDirectory +"\\FileIcon.ico") ;
            FileReg.CreateSubKey("PerceivedType").SetValue("",  "Barcode Templete File ");
            AppReg.CreateSubKey("shell\\open\\command").SetValue("", "\"" + Application.ExecutablePath + "\" %1");
            AppReg.CreateSubKey("shell\\edit\\command").SetValue("", "\"" + Application.ExecutablePath + "\" %1");
            AppReg.CreateSubKey("DefaultIcon").SetValue("", AppDomain.CurrentDomain.BaseDirectory + "\\FileIcon.ico");

            AppASSC.CreateSubKey("UserChoice").SetValue("Progid", "Applecations\\BarcodeMaker.exe");
            SHChangeNotify(0x08000000, 0x0000, IntPtr.Zero, IntPtr.Zero);
            MessageBox.Show("Add The Extention");
        }
    }
}
