﻿using DevExpress.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.BarCode;
using DevExpress.XtraReports.UI;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Linq.Expressions;

namespace BarcodeMaker
{
    partial class rpt_Print
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = (IContainer)null;
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
    
            private void InitializeComponent()
    {
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.lbl_Line_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Line_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Line_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Line_4 = new DevExpress.XtraReports.UI.XRBarCode();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Detail.BorderWidth = 1F;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 100F;
            this.Detail.MultiColumn.ColumnCount = 6;
            this.Detail.MultiColumn.ColumnWidth = 350F;
            this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnWidth;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.StylePriority.UseBorderWidth = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel1.BorderWidth = 1F;
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbl_Line_1,
            this.lbl_Line_2,
            this.lbl_Line_3,
            this.lbl_Line_4});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(350F, 100F);
            this.xrPanel1.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrPanel1.StylePriority.UseBorders = false;
            this.xrPanel1.StylePriority.UseBorderWidth = false;
            this.xrPanel1.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.xrPanel1_PrintOnPage);
            // 
            // lbl_Line_1
            // 
            this.lbl_Line_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbl_Line_1.CanShrink = true;
            this.lbl_Line_1.Dpi = 254F;
            this.lbl_Line_1.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Line_1.LocationFloat = new DevExpress.Utils.PointFloat(13F, 5F);
            this.lbl_Line_1.Multiline = true;
            this.lbl_Line_1.Name = "lbl_Line_1";
            this.lbl_Line_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.lbl_Line_1.SizeF = new System.Drawing.SizeF(330F, 20F);
            this.lbl_Line_1.StylePriority.UseBorders = false;
            this.lbl_Line_1.StylePriority.UseFont = false;
            this.lbl_Line_1.StylePriority.UsePadding = false;
            this.lbl_Line_1.StylePriority.UseTextAlignment = false;
            this.lbl_Line_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Line_2
            // 
            this.lbl_Line_2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbl_Line_2.CanShrink = true;
            this.lbl_Line_2.Dpi = 254F;
            this.lbl_Line_2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Line_2.LocationFloat = new DevExpress.Utils.PointFloat(13F, 25F);
            this.lbl_Line_2.Multiline = true;
            this.lbl_Line_2.Name = "lbl_Line_2";
            this.lbl_Line_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.lbl_Line_2.SizeF = new System.Drawing.SizeF(330F, 20F);
            this.lbl_Line_2.StylePriority.UseBorders = false;
            this.lbl_Line_2.StylePriority.UseFont = false;
            this.lbl_Line_2.StylePriority.UsePadding = false;
            this.lbl_Line_2.StylePriority.UseTextAlignment = false;
            this.lbl_Line_2.Text = " ";
            this.lbl_Line_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Line_3
            // 
            this.lbl_Line_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbl_Line_3.CanShrink = true;
            this.lbl_Line_3.Dpi = 254F;
            this.lbl_Line_3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Line_3.LocationFloat = new DevExpress.Utils.PointFloat(13F, 45F);
            this.lbl_Line_3.Multiline = true;
            this.lbl_Line_3.Name = "lbl_Line_3";
            this.lbl_Line_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.lbl_Line_3.SizeF = new System.Drawing.SizeF(330F, 20F);
            this.lbl_Line_3.StylePriority.UseBorders = false;
            this.lbl_Line_3.StylePriority.UseFont = false;
            this.lbl_Line_3.StylePriority.UsePadding = false;
            this.lbl_Line_3.StylePriority.UseTextAlignment = false;
            this.lbl_Line_3.Text = " ";
            this.lbl_Line_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_Line_4
            // 
            this.lbl_Line_4.Alignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lbl_Line_4.AutoModule = true;
            this.lbl_Line_4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbl_Line_4.BorderWidth = 1F;
            this.lbl_Line_4.Dpi = 254F;
            this.lbl_Line_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Line_4.LocationFloat = new DevExpress.Utils.PointFloat(13F, 65F);
            this.lbl_Line_4.Name = "lbl_Line_4";
            this.lbl_Line_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.lbl_Line_4.SizeF = new System.Drawing.SizeF(330F, 20F);
            this.lbl_Line_4.StylePriority.UseBorders = false;
            this.lbl_Line_4.StylePriority.UseBorderWidth = false;
            this.lbl_Line_4.StylePriority.UseFont = false;
            this.lbl_Line_4.StylePriority.UsePadding = false;
            this.lbl_Line_4.StylePriority.UseTextAlignment = false;
            this.lbl_Line_4.Symbology = code128Generator1;
            this.lbl_Line_4.Text = "0123456789";
            this.lbl_Line_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 0F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 0F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // rpt_Print
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 144;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.SnapGridSize = 31.75F;
            this.Version = "17.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }


        #endregion


        private DetailBand Detail;
        private XRPanel xrPanel1;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRLabel lbl_Line_3;
        private XRBarCode lbl_Line_4;
        private XRLabel lbl_Line_1;
        private XRLabel lbl_Line_2;
    }
}
